
-- INTRODUCTION --

This module keeps history of node alias whenever we update or insert any node. 
node_alias_history table contains all the detail about node.

This module works with new content.

-- REQUIREMENTS --
  No

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:


-- CUSTOMIZATION --

* Access menu from here : admin/config/search/node-alias-history

-- CONTACT --

Current maintainers:
* Ankush Gautam https://www.drupal.org/u/agautam
  email : ankushgautam76@gmail.com
